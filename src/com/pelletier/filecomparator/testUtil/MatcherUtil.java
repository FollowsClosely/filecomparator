package com.pelletier.filecomparator.testUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.testng.collections.CollectionUtils;

import difflib.DiffUtils;
import difflib.Patch;

/**
 * 
 * @author Ryan Pelletier
 *
 */
public class MatcherUtil {
	private String baseDir = null;
	private String diffDir = null;
	
	public MatcherUtil(){}
	
	public Iterator<Object[]> pairFiles(){
		return match(this.baseDir, this.diffDir);
	}
	
	public Iterator<Object[]> pairFiles(String baseDir, String diffDir){
		return match(baseDir, diffDir);
	}
	
	/**
	 * 
	 * @param baseDir
	 * Name of directory that contains the baseline file of the pair.
	 * @param diffDir
	 * Name of directory that contains the non-baseling file of the pair.
	 * @return
	 */
	private Iterator<Object[]> match(String baseDir, String diffDir) {
		
		String path = System.getenv("BASEDIR");	//Passed by Jenkins so that files can be read from absolute path
		if(path != null){
			path = path + '/';
		}else{
			path = "";
		}

		Map<String, File> baseline = getUniqueNames(new File(path + baseDir));
		Map<String, File> results = getUniqueNames(new File(path + diffDir));

		Set<String> union = new TreeSet<String>();
		union.addAll(baseline.keySet());
		union.addAll(results.keySet());

		Set<Object[]> result = new HashSet<Object[]>();
		for (String key : union) {
			File baselineFile = baseline.get(key);
			File resultsFile = results.get(key);
			result.add(new File[] { baselineFile, resultsFile });
		}
		return result.iterator();

	}
	/**
	 * 
	 * @param directory
	 * Name of directory to find unique files in 
	 * @return
	 * Map, key is unique name of file relative to directory, value is actual File object
	 */
	public Map<String, File> getUniqueNames(File directory) {
		if (!directory.isDirectory()) {
			directory = directory.getParentFile();
		}
		Map<String, File> map = new HashMap<String, File>();
		String absolutePath = directory.getAbsolutePath();		
		List<File> baselineFiles = (List<File>) FileUtils.listFiles(directory,TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		for (File file : baselineFiles) {
			String uniqueName = file.getAbsolutePath().replace(absolutePath, "");
			map.put(uniqueName, file);
		}
		return map;
	}

	public void setBaseDir(String baseDir) {
		this.baseDir = baseDir;
	}
	public void setDiffDir(String diffDir) {
		this.diffDir = diffDir;
	}	

}
