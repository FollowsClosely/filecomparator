package com.pelletier.filecomparator;
/**
 * When this resolver is used any Delta that has multiple lines is flagged as an error.
 * @author Ryan Pelletier
 *
 */
public class MultipleLineResolver implements Resolver {

	@Override
	public void resolve(FileDifference fileDifference) {
		if(fileDifference.getDelta().getRevised().getLines().size() > 1){
			fileDifference.setIsError(true);
		}
	}
}
