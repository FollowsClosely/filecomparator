package com.pelletier.filecomparator;


/**
 * 
 * @author Ryan Pelletier
 *
 */
public interface Resolver {	

	public void resolve(FileDifference fileDifference);
	
}
