package com.pelletier.filecomparator;

import difflib.Delta.TYPE;
import java.util.List;
/**
 * Can be configured to flag a difference as an error based on Delta type (Insert, Delete, or Change)
 * @author Ryan Pelletier
 *
 */
public class DeltaTypeResolver implements Resolver {

	/**
	 * List of Delta types that will be flagged as errors
	 */
	List<TYPE> automaticErrorTypes = null;	//I would like to inject this, I don't know if I can though
	
	/**
	 * If the type of Delta on the fileDifference is in the automaticErrorTypes list
	 * then it will be flagged as an error
	 */
	@Override
	public void resolve(FileDifference fileDifference) {
		for(TYPE type : automaticErrorTypes){
			if(fileDifference.getDelta().getType().equals(type)){
				fileDifference.setIsError(true);
				break;
			}
		}
	}

	public void setAutomaticErrorTypes(List<TYPE> automaticErrorTypes) {
		this.automaticErrorTypes = automaticErrorTypes;
	}

}
