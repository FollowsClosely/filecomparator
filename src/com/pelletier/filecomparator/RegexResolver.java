package com.pelletier.filecomparator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Attempts to match the Delta lines of a fileDifference to a regex pattern.
 * If a match is found, the fileDifference is marked as <b>not</b> an error. 
 * 
 * @author Ryan Pelletier
 *
 */
public class RegexResolver implements Resolver {
	Pattern pattern = null; 

	@Override
	public void resolve(FileDifference fileDifference) {
		
		Matcher matcher = pattern.matcher(fileDifference.getDelta().getRevised().getLines().get(0).toString());
		if (matcher.find()) {
			fileDifference.setIsError(false);
		}
	}

	public void setPattern(String pattern) {
		this.pattern = Pattern.compile(pattern);
	}

	
	@Override
	public String toString(){
		return this.pattern.toString();
	}

}
