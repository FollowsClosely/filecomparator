package com.pelletier.filecomparator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import difflib.ChangeDelta;
import difflib.Chunk;

public class RegexResolverTest {
	

	RegexResolver resolver = null;
	FileDifference fileDifference = null;
	List<String> baseLines = null;
	List<String> diffLines = null;
	
	@Before()
	public void setup(){
		resolver = new RegexResolver();
	}
	
	@Test
	public void testResolvingAFileDifference() {
		
		diffLines = new ArrayList<String>();
		diffLines.add(new String("here is a mocked file line (difference)"));
		baseLines = new ArrayList<String>();
		baseLines.add(new String("here is a mocked file line"));
		
		
		fileDifference = new FileDifference();
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
				
		resolver.setPattern("(difference)");
		resolver.resolve(fileDifference);
		
		Assert.assertEquals(fileDifference.getIsError(), new Boolean(false));
	}
	
	@Test
	public void testNotResolvingAFileDifference(){
		diffLines = new ArrayList<String>();
		diffLines.add(new String("here is a mocked file line (difference)"));
		baseLines = new ArrayList<String>();
		baseLines.add(new String("here is a mocked file line"));
		
		
		fileDifference = new FileDifference();
		fileDifference.setDelta(new ChangeDelta<String>(new Chunk<String>(0, baseLines), new Chunk<String>(0,diffLines)));
				
				
		resolver.setPattern("(regex)");
		resolver.resolve(fileDifference);
		
		Assert.assertEquals(fileDifference.getIsError(), null);
		}

}