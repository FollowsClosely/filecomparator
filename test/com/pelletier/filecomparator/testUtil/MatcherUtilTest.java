package com.pelletier.filecomparator.testUtil;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.Iterator;
import java.util.Map;

import org.junit.Test;
import org.testng.Assert;

public class MatcherUtilTest {
	//test getting files where they are unique

	@Test
	public void testGetUniqueNames(){
		MatcherUtil matcherUtil = new MatcherUtil();
		Map<String,File> map = matcherUtil.getUniqueNames(new File("testDirectories\\date_1"));
		File file1 = map.get("\\protocolA\\id_001\\format\\test.txt");
		File file2 = map.get("\\protocolB\\id_001\\format\\test.txt");
		Assert.assertEquals(file1, new File("testDirectories/date_1/protocolA/id_001/format/test.txt"));
		Assert.assertEquals(file2, new File("testDirectories/date_1/protocolB/id_001/format/test.txt"));
	}
	
	//basic test case for matching two files
	@Test
	public void testFindingBasicPair() {
		MatcherUtil matcherUtil= new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectories/testBaseDir_1","testDirectories/testDiffDir_1");
		Object[] pair = pairs.next();
		assertEquals(pair[0].toString(),"testDirectories\\testBaseDir_1\\test.txt");
		assertEquals(pair[1].toString(),"testDirectories\\testDiffDir_1\\test.txt");
	}
	
	//test case for sub-directories still same
	@Test
	public void testFindingPairsInSubdirectories() {
		MatcherUtil matcherUtil= new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectories/testBaseDir_2","testDirectories/testDiffDir_2");
		Object[] pair = pairs.next();
		Object[] pair2 = pairs.next();
		
		assertEquals(pair[0].toString(),"testDirectories\\testBaseDir_2\\hello.txt");
		assertEquals(pair[1].toString(),"testDirectories\\testDiffDir_2\\hello.txt");
		assertEquals(pair2[0].toString(),"testDirectories\\testBaseDir_2\\subdir\\test.txt");
		assertEquals(pair2[1].toString(),"testDirectories\\testDiffDir_2\\subdir\\test.txt");
	}
	
	//test case for extra/missing file
	@Test
	public void testFindingBasicPairWithMissingFile() {
		MatcherUtil matcherUtil= new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectories/testBaseDir_3","testDirectories/testDiffDir_3");
		Object[] pair = pairs.next();
		Object[] pair2 = pairs.next();
		
		assertEquals(pair[0].toString(),"testDirectories\\testBaseDir_3\\test2.txt");
		assertEquals(pair[1],null);
		assertEquals(pair2[0].toString(), "testDirectories\\testBaseDir_3\\test1.txt");
		assertEquals(pair2[1].toString(), "testDirectories\\testDiffDir_3\\test1.txt");
	}
	
	//test case for extra/missing file in sub-directory
	@Test
	public void finalTest(){
		MatcherUtil matcherUtil = new MatcherUtil();
		Iterator<Object[]> pairs = matcherUtil.pairFiles("testDirectories/testBaseDir_4","testDirectories/testDiffDir_4");
		Object[] pair = pairs.next();
		Object[] pair2 = pairs.next();
		
		assertEquals(pair2[0], null);
		assertEquals(pair2[1].toString(),"testDirectories\\testDiffDir_4\\subdir\\test2.txt");
		assertEquals(pair[0].toString(),"testDirectories\\testBaseDir_4\\subdir\\test.txt");
		assertEquals(pair[1].toString(),"testDirectories\\testDiffDir_4\\subdir\\test.txt");
	}

}
